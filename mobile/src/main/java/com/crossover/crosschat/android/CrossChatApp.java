package com.crossover.crosschat.android;

import com.arellomobile.mvp.RegisterMoxyReflectorPackages;
import com.crossover.crosschat.android.core.CoreApplication;

/**
 * Created by Mahmoud Abdurrahman (mahmoud.abdurrahman@crossover.com) on 2/8/18.
 */
@RegisterMoxyReflectorPackages("com.crossover.crosschat.android.core.moxy")
public class CrossChatApp extends CoreApplication {

    public static final String TAG = CrossChatApp.class.getSimpleName();

    private static CrossChatApp instance;

    public CrossChatApp() {
        instance = this;
    }

    public static CrossChatApp getInstance() {
        return instance;
    }
}
